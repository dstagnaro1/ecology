## This is my ecology discussion question

a) The “1.2” coefficient means that for every mouse, one cycle later, there are 1.2 more mice
The “-0.001” means that for every owl mouse combo, that many dies or is eaten
The “0.7” means that for every owl, after one cycle, there are 0.7 more owls
The “+0.002” means that for every owl mouse combo, that many more survive and don’t die

b) This is in the code. There is also a screenshot

c) After experimenting with various values for both, they tend to either die out or survive well, but not really a rhyme or reason as to what happens