# m_n+1 = 1.2m_n - 0.001*o_n*m_n
# o_n+1 = 0.7o_n + 0.002*o_n*m_n

# m = 1.2m - 0.001*o*m
# o = 0.7o + 0.002*o*m

# 0 = 0.2m - 0.001*o*m
# 0 = -0.3o + 0.002*o*m

# 0 = m(0.2 - 0.001*o)
# 0 = o(-0.3 + 0.002*m)

# Equilibrium: Mice = 200; Owl = 150

# owl = 150
# mice = 200


def ecology(owl, mice):
    loops = 0
    while True:
        # for i in range(100):
        new_mice = 1.2 * mice - 0.001 * owl * mice
        new_owl = 0.7 * owl + 0.002 * owl * mice

        mice = new_mice
        owl = new_owl

        loops += 1

        if (new_owl < 1) or (new_mice < 1):
            print("After {} years, the ecology of this wildlife sanctuary will fail".format(loops))
            print("Owls {}\nMice {}\n".format(mice, owl))
            break

        if loops > 1000:
            print("This is just a computer program, and this would break the computer otherwise")
            break

            # print("After {} years, the ecology of this wildlife sanctuary will fail".format(loops))
            # print("Owls {}\nMice {}\n".format(round(mice, 0), round(owl, 0)))
            # print("Owls {}\nMice {}\n".format(mice, owl))


ecology(150, 200)
ecology(150, 300)
ecology(100, 200)
ecology(10, 20)
ecology(200, 150)
